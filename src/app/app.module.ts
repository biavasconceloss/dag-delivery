import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AboutComponent } from './components/about/about.component';
import { ListComponent } from './components/list/list.component';
import { OrdersComponent } from './components/orders/orders.component';

import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import {MatDialogModule} from '@angular/material/dialog';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatPaginator, MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';

import { AuthService } from './services/auth.service';
import { PedidosService } from './services/pedidos.service';
import { MatPaginatorIntlCro } from './translate/customClass';
import { OrdersModalComponent } from './components/orders/orders-modal/orders-modal.component';
import { NfModalComponent } from './components/orders/nf-modal/nf-modal.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    AboutComponent,
    ListComponent,
    OrdersComponent,
    OrdersModalComponent,
    NfModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatGridListModule,
    MatTableModule,
    MatExpansionModule,
    MatPaginatorModule,
    HttpClientModule,
    MatDialogModule,
    MatMenuModule,
    MatCardModule
  ],
  providers: [
    AuthService,
    MatPaginatorIntl,
    MatPaginatorIntlCro,
    { provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro },],
  bootstrap: [AppComponent]
})
export class AppModule { }
