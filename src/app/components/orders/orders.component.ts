import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Pedido } from 'src/app/models/pedido';
import { PedidosService } from 'src/app/services/pedidos.service';
import { NfModalComponent } from './nf-modal/nf-modal.component';
import { OrdersModalComponent } from './orders-modal/orders-modal.component';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  pedidoId: number;
  pedido: Pedido;

  constructor(public actRoute: ActivatedRoute, public router: Router,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.pedidoId = this.actRoute.snapshot.params['id'];
    this.getPedido();
  }

  getPedido(){
    if(this.pedidoId == 1){
      this.pedido = {"id": 1,"numeroPedido": "9CV257",
      "orcPedido": "845247",
      "statusPedido": "RECEBIDO",
      "previsaoEntrega": "6 de agosto de 2021",
      "valorTotal": "R$1.280,56",
      "totalDescontos": "R$58,15",
      "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
      "formaPagamento": "Boleto de Pagamento - 14 dias",
      "itensPedido": [
          {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
          {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
      ],
      "notaFiscal": 
          {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.280,56"} }
    }

    if(this.pedidoId == 2) {
      this.pedido = {
        "id": 2,
        "numeroPedido": "9CV258",
        "orcPedido": "845248",
        "statusPedido": "APROVADO",
        "previsaoEntrega": "6 de agosto de 2021",
        "valorTotal": "R$1.280,56",
        "totalDescontos": "R$58,15",
        "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
        "formaPagamento": "Boleto de Pagamento - 14 dias",
        "itensPedido": [
            {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
            {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
        ],
        "notaFiscal": 
            {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.280,56"}
        
      }

     
    }

    if(this.pedidoId == 3) {
      this.pedido =  { 
        "id": 3,
        "numeroPedido": "9CV259",
        "orcPedido": "845249",
        "statusPedido": "SEPARADO",
        "previsaoEntrega": "6 de agosto de 2021",
        "valorTotal": "R$1.260,56",
        "totalDescontos": "R$58,15",
        "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
        "formaPagamento": "Boleto de Pagamento - 14 dias",
        "itensPedido": [
            {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
            {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
        ],
        "notaFiscal": 
            {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.260,56"}
        
      }
    }

    if(this.pedidoId == 4) {
      this.pedido = {
        "id": 4,
        "numeroPedido": "9CV260",
        "orcPedido": "845250",
        "statusPedido": "EM TRANSPORTE",
        "previsaoEntrega": "6 de agosto de 2021",
        "valorTotal": "R$1.260,56",
        "totalDescontos": "R$58,15",
        "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
        "formaPagamento": "Boleto de Pagamento - 14 dias",
        "itensPedido": [
            {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
            {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
        ],
        "notaFiscal": {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.260,56"}
        
      }
    }
    
    if(this.pedidoId == 5) {
      this.pedido = {
        "id": 5,
        "numeroPedido": "9CV261",
        "orcPedido": "845251",
        "statusPedido": "ENTREGUE",
        "previsaoEntrega": "6 de agosto de 2021",
        "valorTotal": "R$1.260,56",
        "totalDescontos": "R$58,15",
        "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
        "formaPagamento": "Boleto de Pagamento - 14 dias",
        "itensPedido": [
            {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
            {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
        ],
        "notaFiscal": 
            {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.260,56"}
        
      }
    }
    if(this.pedidoId == 6) {
      this.pedido = { "id": 6,
      "numeroPedido": "87X257",
      "orcPedido": "845347",
      "statusPedido": "RECEBIDO",
      "previsaoEntrega": "6 de agosto de 2021",
      "valorTotal": "R$1.120,56",
      "totalDescontos": "R$58,15",
      "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
      "formaPagamento": "Boleto de Pagamento - 14 dias",
      "itensPedido": [
          {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
          {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
      ],
      "notaFiscal": 
          {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.280,56"}
      
    }
    }
  }

  openDialog(data) {
    const dialogRef = this.dialog.open(OrdersModalComponent, {
      width: '250px',
      data: this.pedido
    });
  }

  openDialogNf(data) {
    const dialogRef = this.dialog.open(NfModalComponent, {
      width: '250px',
      data: this.pedido
    });
  }

}
