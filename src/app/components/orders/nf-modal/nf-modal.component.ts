import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pedido } from 'src/app/models/pedido';

@Component({
  selector: 'app-nf-modal',
  templateUrl: './nf-modal.component.html',
  styleUrls: ['./nf-modal.component.css']
})
export class NfModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<NfModalComponent>,
    @Inject (MAT_DIALOG_DATA) public data: Pedido) { }

  ngOnInit(): void {
  }

}
