import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NfModalComponent } from './nf-modal.component';

describe('NfModalComponent', () => {
  let component: NfModalComponent;
  let fixture: ComponentFixture<NfModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NfModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NfModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
