import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Pedido } from 'src/app/models/pedido';

@Component({
  selector: 'app-orders-modal',
  templateUrl: './orders-modal.component.html',
  styleUrls: ['./orders-modal.component.css']
})
export class OrdersModalComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<OrdersModalComponent>,
     @Inject (MAT_DIALOG_DATA) public data: Pedido) { 
     }

  ngOnInit(): void {
  }

}
