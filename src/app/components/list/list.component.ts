import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Pedido } from 'src/app/models/pedido';
import { PedidosService } from 'src/app/services/pedidos.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  panelOpenRecebido = false;
  panelOpenAprovado = false;
  panelOpenSeparado = false;
  panelOpenEmTransporte = false;
  panelOpenEntregue = false;

  displayedColumnsRecebido: string[] = [
    'numeroPedido',
    'statusPedido',
    'valorTotal',
    'actions',
  ];

  displayedColumnsAprovado: string[] = [
    'numeroPedido',
    'statusPedido',
    'valorTotal',
    'actions',
  ];

  displayedColumnsSeparado: string[] = [
    'numeroPedido',
    'statusPedido',
    'valorTotal',
    'actions',
  ];

  displayedColumnsEmTransporte: string[] = [
    'numeroPedido',
    'statusPedido',
    'valorTotal',
    'actions',
  ];

  displayedColumnsEntregue: string[] = [
    'numeroPedido',
    'statusPedido',
    'valorTotal',
    'actions',
  ];

  ELEMENT_DATA: Pedido[] = [
    {"id": 1,
    "numeroPedido": "9CV257",
    "orcPedido": "845247",
    "statusPedido": "RECEBIDO",
    "previsaoEntrega": "6 de agosto de 2021",
    "valorTotal": "R$1.280,56",
    "totalDescontos": "R$58,15",
    "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
    "formaPagamento": "Boleto de Pagamento - 14 dias",
    "itensPedido": [
        {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
        {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
    ],
    "notaFiscal": 
        {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.280,56"}
    
  },
  {
    "id": 2,
    "numeroPedido": "9CV258",
    "orcPedido": "845248",
    "statusPedido": "APROVADO",
    "previsaoEntrega": "6 de agosto de 2021",
    "valorTotal": "R$1.280,56",
    "totalDescontos": "R$58,15",
    "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
    "formaPagamento": "Boleto de Pagamento - 14 dias",
    "itensPedido": [
        {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
        {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
    ],
    "notaFiscal": 
        {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.280,56"}
    
  },
  { 
    "id": 3,
    "numeroPedido": "9CV259",
    "orcPedido": "845249",
    "statusPedido": "SEPARADO",
    "previsaoEntrega": "6 de agosto de 2021",
    "valorTotal": "R$1.260,56",
    "totalDescontos": "R$58,15",
    "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
    "formaPagamento": "Boleto de Pagamento - 14 dias",
    "itensPedido": [
        {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
        {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
    ],
    "notaFiscal": 
        {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.260,56"}
    
  },
  {
    "id": 4,
    "numeroPedido": "9CV260",
    "orcPedido": "845250",
    "statusPedido": "EM TRANSPORTE",
    "previsaoEntrega": "6 de agosto de 2021",
    "valorTotal": "R$1.260,56",
    "totalDescontos": "R$58,15",
    "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
    "formaPagamento": "Boleto de Pagamento - 14 dias",
    "itensPedido": [
        {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
        {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
    ],
    "notaFiscal": {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.260,56"}
    
  },
  {
    "id": 5,
    "numeroPedido": "9CV261",
    "orcPedido": "845251",
    "statusPedido": "ENTREGUE",
    "previsaoEntrega": "6 de agosto de 2021",
    "valorTotal": "R$1.260,56",
    "totalDescontos": "R$58,15",
    "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
    "formaPagamento": "Boleto de Pagamento - 14 dias",
    "itensPedido": [
        {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
        {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
    ],
    "notaFiscal": 
        {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.260,56"}
    
  },
  { "id": 6,
    "numeroPedido": "87X257",
    "orcPedido": "845347",
    "statusPedido": "RECEBIDO",
    "previsaoEntrega": "6 de agosto de 2021",
    "valorTotal": "R$1.120,56",
    "totalDescontos": "R$58,15",
    "enderecoEntrega": "QUARTO ANEL VIÁRIO, 2165 - FORTALEZA/CE",
    "formaPagamento": "Boleto de Pagamento - 14 dias",
    "itensPedido": [
        {"nomeItem": "Kit Salon OPUS - SOS Desmaia Fios", "quantidadeItem": "15"},
        {"nomeItem": "Kit Dove - Poder das Plantas", "quantidadeItem": "12"}
    ],
    "notaFiscal": 
        {"identificacaoEmitente": "DAG Distribuidora", "cnpj": "07.216.054/0001-38", "valorTotalDaNota": "R$1.280,56"}
    
  }

  ];

  dataSourceRecebido = new MatTableDataSource<Pedido>(this.ELEMENT_DATA);
  dataSourceAprovado = new MatTableDataSource<Pedido>(this.ELEMENT_DATA);
  dataSourceSeparado = new MatTableDataSource<Pedido>(this.ELEMENT_DATA);
  dataSourceEmTransporte = new MatTableDataSource<Pedido>(this.ELEMENT_DATA);
  dataSourceEntregue = new MatTableDataSource<Pedido>(this.ELEMENT_DATA);

  @ViewChildren(MatPaginator)
  paginator = new QueryList<MatPaginator>();

  @ViewChildren(MatSort) sort = new QueryList<MatSort>();

  constructor() { }

  ngAfterViewInit(): void {
    this.dataSourceRecebido.paginator = this.paginator.toArray()[0];
    this.dataSourceRecebido.sort = this.sort.toArray()[0];
    this.dataSourceAprovado.paginator = this.paginator.toArray()[1];
    this.dataSourceAprovado.sort = this.sort.toArray()[1];
    this.dataSourceSeparado.paginator = this.paginator.toArray()[2];
    this.dataSourceSeparado.sort = this.sort.toArray()[2];
    this.dataSourceEmTransporte.paginator = this.paginator.toArray()[3];
    this.dataSourceEmTransporte.sort = this.sort.toArray()[3];
    this.dataSourceEntregue.paginator = this.paginator.toArray()[4];
    this.dataSourceEntregue.sort = this.sort.toArray()[4];
  }

  ngOnInit(): void {
    this.getPedidos();
  }

  getPedidos(){
    
      this.dataSourceRecebido.data = this.dataSourceRecebido.data.filter((x) => x.statusPedido == 'RECEBIDO');
      this.dataSourceAprovado.data = this.dataSourceAprovado.data.filter((x) => x.statusPedido == 'APROVADO');
      this.dataSourceSeparado.data = this.dataSourceSeparado.data.filter((x) => x.statusPedido == 'SEPARADO');
      this.dataSourceEmTransporte.data = this.dataSourceEmTransporte.data.filter((x) => x.statusPedido == 'EM TRANSPORTE');
      this.dataSourceEntregue.data = this.dataSourceEntregue.data.filter((x) => x.statusPedido == 'ENTREGUE');
    
  }

  

}
