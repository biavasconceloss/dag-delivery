import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Pedido } from '../models/pedido';

@Injectable()
export class PedidosService {

  /* url = 'http://localhost:3000/'; // API REST COM JSON-SERVER

  constructor(private http: HttpClient) { }

   // HEADERS
   httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getPedidos(): Observable<Pedido[]> {
    return this.http
      .get<Pedido[]>(this.url + 'pedidos')
      .pipe(catchError(this.handleError));
  }

  // TRAZ TASK POR ID
  getPedido(id: number): Observable<Pedido> {
    return this.http
      .get<Pedido>(this.url + 'pedidos/' + id)
      .pipe(catchError(this.handleError));
  } */

  // MANIPULADOR DE ERRO
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // CLIENT-SIDE ERROR
      errorMessage = error.error.message;
    } else {
      // SERVER-SIDE ERROR
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
