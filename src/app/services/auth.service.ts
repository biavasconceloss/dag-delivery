import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  usuarioAutenticado: boolean = false;

  constructor(private router: Router) { }

  fazerLogin(usuario: Usuario){
    if(usuario.email === "beatriz@gmail.com" && usuario.password === "123456") {
      this.usuarioAutenticado = true;
      this.router.navigate(['/list'])
    } else {
      this.usuarioAutenticado = false;
    }
  }
}
