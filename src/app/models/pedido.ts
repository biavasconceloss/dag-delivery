export interface Pedido {
    id: number;
    numeroPedido: String;
    orcPedido: String;
    statusPedido: String;
    previsaoEntrega: String;
    valorTotal: String;
    totalDescontos: String;
    enderecoEntrega: String;
    formaPagamento: String;
    itensPedido: Array<ItemPedido>;
    notaFiscal: NotaFiscal;
}

export interface ItemPedido {
    nomeItem: String;
    quantidadeItem: String;
}

export interface NotaFiscal {
    identificacaoEmitente: String;
    cnpj: String;
    valorTotalDaNota: String;
}