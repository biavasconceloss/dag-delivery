import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.dag.app',
  appName: 'dag-delivery',
  webDir: 'dist/dag-delivery',
  bundledWebRuntime: false
};

export default config;
