# DagDelivery

Este projeto foi desenvolvido com [Angular CLI](https://github.com/angular/angular-cli) versão 11.2.2.

### Tecnologias utilizadas

- VScode
- Angular
- Angular Material
- NodeJS
- CapacitorJS
- BitBucket

## 🚀 Instalando DAG Delivery

Para instalar o DAG Delivery, siga estas etapas:

Instalar:
```
npm install
```

Executar:
```
ng serve
```


